package com.jeesite.maven.plugin.compressor;

import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

import org.apache.maven.plugin.logging.Log;

import com.google.javascript.jscomp.CommandLineRunner;

/**
 * GoogleCompiler
 * @author ThinkGem
 * @version 2020-3-9
 */
public class GoogleCompiler extends CommandLineRunner {
	
	private static GoogleCompiler compiler;

	protected GoogleCompiler(List<String> a) {
		super(a.toArray(new String[a.size()]));
	}
	
	public void update(List<String> a) {
		String[] args = a.toArray(new String[a.size()]);
		ReflectUtils.invokeMethod(this, "initConfigFromFlags",
				new Class<?>[] {
						String[].class, PrintStream.class, PrintStream.class
				}, new Object[] {
						args, System.out, System.err
				});
	}

	public void run(Log log) {
		if (this.shouldRunCompiler()) {
			try {
				this.doRun();
			} catch (IOException e) {
				log.error(e);
			}
		}
	}
	
	public static void run(List<String> a, Log log) {

		log.debug("compiler: " + a.toString());
		
		if (compiler == null) {
			compiler = new GoogleCompiler(a);
		}else {
			compiler.update(a);
		}
		
		compiler.run(log);
	}
	
	public static void main(String[] args) {
		CommandLineRunner.main(new String[] {"--help"});
	}
	
}

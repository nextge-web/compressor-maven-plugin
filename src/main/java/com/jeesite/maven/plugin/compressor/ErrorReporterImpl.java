package com.jeesite.maven.plugin.compressor;

import java.io.File;
import org.apache.maven.plugin.logging.Log;
import org.mozilla.javascript.ErrorReporter;
import org.mozilla.javascript.EvaluatorException;
import org.sonatype.plexus.build.incremental.BuildContext;

public class ErrorReporterImpl implements ErrorReporter {

	private Log log;
	private boolean acceptWarn;
	private int warningCnt;
	private int errorCnt;
	private BuildContext buildContext;
	private String defaultFileName;
	private File sourceFile;

	public ErrorReporterImpl(Log log, boolean jswarn, BuildContext buildContext) {
		this.log = log;
		this.acceptWarn = jswarn;
		this.buildContext = buildContext;
	}

	public void setDefaultFileName(String v) {
		if (v.length() == 0) {
			v = null;
		}
		defaultFileName = v;
	}

	public int getErrorCnt() {
		return errorCnt;
	}

	public int getWarningCnt() {
		return warningCnt;
	}

	public void error(String message, String sourceName, int line, String lineSource, int lineOffset) {
		String fullMessage = newMessage(message, sourceName, line, lineSource, lineOffset);
		buildContext.addMessage(sourceFile, line, lineOffset, message, BuildContext.SEVERITY_ERROR, null);
		log.error(fullMessage);
		errorCnt++;
	}

	public EvaluatorException runtimeError(String message, String sourceName, int line, String lineSource, int lineOffset) {
		error(message, sourceName, line, lineSource, lineOffset);
		throw new EvaluatorException(message, sourceName, line, lineSource, lineOffset);
	}

	public void warning(String message, String sourceName, int line, String lineSource, int lineOffset) {
		if (acceptWarn) {
			String fullMessage = newMessage(message, sourceName, line, lineSource, lineOffset);
			buildContext.addMessage(sourceFile, line, lineOffset, message, BuildContext.SEVERITY_WARNING, null);
			log.warn(fullMessage);
			warningCnt++;
		}
	}

	private String newMessage(String message, String sourceName, int line, String lineSource, int lineOffset) {
		StringBuilder back = new StringBuilder();
		if ((sourceName == null) || (sourceName.length() == 0)) {
			sourceName = defaultFileName;
		}
		if (sourceName != null) {
			back.append(sourceName).append(":line ").append(line).append(":column ").append(lineOffset).append(':');
		}
		if ((message != null) && (message.length() != 0)) {
			back.append(message);
		} else {
			back.append("unknown error");
		}
		if ((lineSource != null) && (lineSource.length() != 0)) {
			back.append("\n\t").append(lineSource);
		}
		return back.toString();
	}

	public void setFile(File file) {
		sourceFile = file;
	}

}

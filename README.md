# compressor-maven-plugin

#### 介绍

是一个压缩 JS / CSS 的 Maven 插件，它是 Google Closure Compiler（压缩JS）和 YUI Compressor（压缩CSS） 的合体，
目前支持 ES2015、ES2016、ES2017、ES2018、ES2019

本项目地址：<https://gitee.com/thinkgem/compressor-maven-plugin>

* Forked From: <https://github.com/davidB/yuicompressor-maven-plugin>
* Closure Compiler: <https://github.com/google/closure-compiler>
* YUI compressor: <https://github.com/yui/yuicompressor>

由于时间有限，暂时实现了只 Closure Compiler 的整合，基础用法，在批量处理的情况下还待性能优化，另外还有一些参数需要开放配置选项。

看到这个项目的小伙伴，如果有时间可以继续完善此项目，赶快 Pull 我吧。

#### 编译打包

执行 `bin/package.bat` 文件，打包并将插件安装到本地 Maven 仓库。

#### 使用说明
 
```xml
<!-- JavaScript CSS Compress -->
<plugin>
	<groupId>com.jeesite.maven</groupId>
	<artifactId>compressor-maven-plugin</artifactId>
	<version>1.0.0-SNAPSHOT</version>
	<executions>
		<execution>
			<phase>prepare-package</phase>
			<goals>
				<goal>compress</goal>
			</goals>
		</execution>
	</executions>
	<configuration>
		<jswarn>false</jswarn>
		<nosuffix>true</nosuffix>
		<force>true</force>
		<includes>
			<include>**/*.js</include>
			<include>**/*.css</include>
		</includes>
		<excludes>
			<exclude>**/*.min.js</exclude>
			<exclude>**/*.min.css</exclude>
		</excludes>
	</configuration>
</plugin>
```

# 技术交流方式

* QQ 群号：`127515876`、`209330483`、`223507718`、`709534275`、`730390092`
* 问题反馈：<https://gitee.com/thinkgem/compressor-maven-plugin/issues>
* 作者博客：<https://my.oschina.net/thinkgem>
* **开发平台**：<http://jeesite.com>
* 微信公众号：

![JeeSite4微信公众号](https://static.oschina.net/uploads/space/2018/0302/145133_OGZf_941661.jpg "JeeSite4微信公众号")
